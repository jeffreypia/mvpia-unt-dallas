var gk = 'AIzaSyB3WN9wmajthVvJ_wcy3TarwgKRO12E6EA';

// Utility function to determine if in smallest viewport
function isMobile(width) {
	if( width == undefined ) {
		// Should match smallest viewport check in CSS
		width = 768;
	}
	if( window.innerWidth <= width ) {
		return true;
	} else {
		return false;
	}
}

$('.no-js').removeClass('no-js').addClass('js');

// Toggle primary and subnavs for mobile
$('#header').odToggleNav({
	toggle: '.header-nav__toggle',
	activeClass: 'header-nav__toggle--toggled',
	hideOverflow: true
}).odToggleNav({
	toggle: '.header-nav__sub-toggle',
	activeClass: 'header-nav__sub-toggle--toggled'
});

// Initialize sliders
$('.slider').odSlider();

// Get address and wrap map image in a link to Google Maps, Directions page
var $map = $('#map');
$map.parent().wrapInner('<a target="_blank" classSelected = "map__link" href="' + 'https://www.google.com/maps?saddr=My+Location&daddr=' + encodeURIComponent( $map.data('address') ) + '">')

var $map2 = $('#map2');
$map2.odMap2();
$map2.siblings('.page-title').wrap('<a target="_blank" classSelected = "map__link" href="' + 'https://www.google.com/maps?saddr=My+Location&daddr=' + encodeURIComponent( $map.data('address') ) + '">');

// Initialize filters
$('#filtersDirectory').odToggleFilter({
	toggles: '[data-toggle]',
	itemsContainer: '#stores',
	items: '[data-category]'
});

// Initialize date picker
$('#calendarDatePicker').odDatePicker();
