// FED Paths
// var BASE_FED = "FED";
var FED_BUILD = "fed";
var JS_FED_SRC = FED_BUILD + "/js";
var JSVENDOR_FED = FED_BUILD + "/js-vendor";
var SASS_FED = FED_BUILD + "/sass";
var PUG_FED = FED_BUILD + "/pug";
var SVGS_FED = FED_BUILD + "/svgs";
var IMAGES_FED_SRC = FED_BUILD + "/images";
var CONTENT_FED_SRC = FED_BUILD + "/content";
var ICONS_FED_SRC = FED_BUILD + "/icons";

// Compiled files (unminified)
var ASSETS_FED = FED_BUILD + "/assets";
var CSS_FED = ASSETS_FED + "/css";
var IMAGES_FED = ASSETS_FED + "/images";
var CONTENT_FED = ASSETS_FED + "/content";
var ICONS_FED = ASSETS_FED + "/icons";
var FONTS_FED = ASSETS_FED + "/fonts";
var JS_FED = ASSETS_FED + "/js";
var HTML_FED = FED_BUILD + "/html";
var BASE_SERVER = __dirname + "/" + PUG_FED + "/templates/";

// FED File Paths
var JS_FILES = JS_FED_SRC + "/**/*.js";
var SASS_FILES = SASS_FED + "/**/*.scss";
var IMAGES_FILES = IMAGES_FED_SRC + "/**/*.*";
var CONTENT_FILES = CONTENT_FED_SRC + "/**/*.*";
var ICONS_FILES = ICONS_FED_SRC + "/**/*.*";
var FONTS_FILES = FONTS_FED + "/**/*.*";
var SVGS_FILES = SVGS_FED + "/**/*.svg";
var PUG_FILES = PUG_FED + "/**/*.pug";
var CSS_FILES = CSS_FED + "/**/*.css";
var JSVENDOR_FILES = JSVENDOR_FED + "/**/*.*";

// Deploy Paths (minified)
var BASE_DEPLOY = 'deploy';
var ASSETS_DEPLOY = BASE_DEPLOY + "/assets";
var CSS_DEPLOY = ASSETS_DEPLOY + "/css";
var IMAGES_DEPLOY = ASSETS_DEPLOY + "/images";
var CONTENT_DEPLOY = ASSETS_DEPLOY + "/content";
var ICONS_DEPLOY = ASSETS_DEPLOY + "/icons";
var FONTS_DEPLOY = ASSETS_DEPLOY + "/fonts";
var JS_DEPLOY = ASSETS_DEPLOY + "/js";
var SVGS_DEPLOY = ASSETS_DEPLOY + "/icons";
var PDF_DEPLOY = ASSETS_DEPLOY + "/pdf";
var HTML_DEPLOY = BASE_DEPLOY + "/html";

// Include gulp/gulp plugins
var gulp = require('gulp');
var notify = require('gulp-notify');

//Other Plugins
var pug = require('pug');
var gulpPug = require('gulp-pug');
var sourcemaps = require('gulp-sourcemaps');
var bs = require("browser-sync").create();
var reload = bs.reload;
var concat = require('gulp-concat');

var setupExpress = function() {
	var express = require('express');
	var router  = express.Router();
	var app = express();
	app.use(express.static(FED_BUILD));
	app.set('view engine', 'pug');
	app.set('views', BASE_SERVER);
	app.locals.platform = "desktop";

	// base route
	router.get('/', function(req, res) {
		app.locals.rtl = 0;

		if ("rtl" in req.query) {
			app.locals.rtl = 1;
		}
		return res.render("index");
	});

	// base route for template files
	router.get('/:fileName', function(req, res) {
		app.locals.rtl = 0;

		if ("rtl" in req.query) {
			app.locals.rtl = 1;
		}
		return res.render(req.params.fileName);
	});

  // set routes to app
  app.use(router);

  return app;
};

// Sass Task
gulp.task('build-sass', function() {
	var gulpSass = require('gulp-sass');
	var bourbon = require('node-bourbon');
	var sassOptions = {
		errLogToConsole: true,
		linefeed: 'lf', // 'lf'/'crlf'/'cr'/'lfcr'
		outputStyle: 'expanded', // 'nested','expanded','compact','compressed'
		sourceComments: 'map',
		sourceMap: SASS_FED,
		includePaths: bourbon.includePaths
	};

	return gulp.src(SASS_FILES)
		// .pipe(sourcemaps.init())
		.pipe(gulpSass(sassOptions))
		.on("error", notify.onError({
			message: 'Error: <%= error.message %>'
		}))
		// .pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest(CSS_FED))
		.pipe(gulpSass({outputStyle: 'compressed'}))
		.pipe(gulp.dest(CSS_DEPLOY))
		.pipe(bs.stream({match: '**/*.css'}));
});

// Minify Images
gulp.task('build-images', function() {
	var imagemin = require('gulp-imagemin');
	var pngcrush = require('imagemin-pngcrush');
	return gulp.src(IMAGES_FILES)
		.pipe(imagemin({
				progressive: true,
				svgoPlugins: [{removeViewBox: false}],
				use: [pngcrush()]
		}))
		.pipe(gulp.dest(IMAGES_FED))
		.pipe(gulp.dest(IMAGES_DEPLOY));
});

// BEM


// Pug Watch
gulp.task('pug-watch', function(){
	reload();
	return false;
});

// JS Task
gulp.task('build-js', function() {
	var babel = require('gulp-babel');
	var uglify = require('gulp-uglify');

	// Copy over any JS files aren't getting bundled into main
	gulp
		.src([
			JS_FED_SRC + '/*.js',
			'!'+JS_FED_SRC + '/main.js'
		])
		.pipe(gulp.dest(JS_FED))
		.pipe(gulp.dest(JS_DEPLOY))

	// Create temp JS file with babel'd custom JS modules
	return gulp
		.src([
			JS_FED_SRC + '/modules/*.js',
			JS_FED_SRC + '/main.js'
		])
		
		.pipe(concat('main.js'))
		.pipe(babel({presets: ['es2015']}))
		.on("error", function(error) {
				// If you want details of the error in the console
				console.log(error.toString())
				this.emit('end')
		})
		.pipe(gulp.dest(JS_FED))
		.pipe(concat('main.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(JS_FED))
		.on('end', function(){
			// Concatenate vendor files with unminified custom JS file and refresh bs
			console.log("Concatenate vendor files with unminified custom JS file and refresh bs")
			return gulp
				.src([
					JS_FED_SRC + '-vendor/*.js',
					JS_FED + '/main.js'
				])
				.on("error", function(error) {
					 // If you want details of the error in the console
					  console.log(error.toString())
					  this.emit('end')
				})
				.pipe(concat('main.js'))
				.pipe(gulp.dest(JS_FED))
				.on('end', function(){
					// Concatenate vendor files with minified custom JS file and send to deploy folder
					return gulp
						.src([
							JS_FED_SRC + '-vendor/*.js',
							JS_FED + '/main.min.js'])
						.on("error", function(error) {
							 // If you want details of the error in the console
							  console.log(error.toString())
							  this.emit('end')
						})
						.pipe(concat('main.min.js'))
						.pipe(gulp.dest(JS_DEPLOY))
				})
				.pipe(bs.stream());
		});
	
});

// Sprite
gulp.task('build-sprite', function() {
	var svgmin = require('gulp-svgmin');
	var svgSprite = require('gulp-svg-sprite');

	return gulp.src(SVGS_FILES)
		.pipe(svgmin())
		.pipe(svgSprite({
			"log": "debug",
			"shape": {
				"dimension": {
					"maxHeight": 68
				}
			},
			"mode": {
				"symbol": {
					"dest": "./images",
					"sprite": "sprite.svg",
					"example": {
						"dest": "../svgs/sprite.html"
					}
				}
			}}))
		.pipe(gulp.dest(ASSETS_FED))
		.pipe(gulp.dest(ASSETS_DEPLOY))
		.on('end', function() {
			// Reload server when sprite is updated
			reload();
		});

});

// Build Templates
gulp.task('build-templates', function() {
	return gulp.src(PUG_FED + "/templates/*.pug")
		.pipe(gulpPug({
			pretty: true,
			locals: {
				platform: 'desktop'			
			}
		}))
		.pipe(gulp.dest(HTML_DEPLOY))
		.pipe(gulp.dest(HTML_FED));
});

gulp.task('copy-content', function() {
	return gulp.src(CONTENT_FILES)
		.pipe(gulp.dest(CONTENT_FED))
		.pipe(gulp.dest(CONTENT_DEPLOY))
})

gulp.task('copy-icons', function() {
	return gulp.src(ICONS_FILES)
		.pipe(gulp.dest(ICONS_FED))
		.pipe(gulp.dest(ICONS_DEPLOY))
})

// Start Server
gulp.task('server', function() {	
	var app = setupExpress();

	return bs.init({
      server: BASE_SERVER,
      middleware: [app]
  });
});

// Watch Templates Files
gulp.task('watch', function() {
	 gulp.watch(JSVENDOR_FILES, ['build-js']);
	 gulp.watch(JS_FILES, ['build-js']);
	 gulp.watch(SASS_FILES, ['build-sass']);
	 gulp.watch(PUG_FILES, ['pug-watch']);
	 gulp.watch(CONTENT_FILES, ['copy-content']);
	 gulp.watch(ICONS_FILES, ['copy-icons']);
});

// Tasks
gulp.task('build', ['build-sprite', 'build-sass', 'build-js', 'build-images', 'build-templates', 'copy-content', 'copy-icons']);

gulp.task('default', ['server', 'watch']);
